
echo '-----------------------------------------------------------------------------------------------------'
echo 'gst python'

wget http://ftp.de.debian.org/debian/pool/main/g/gst-python1.0/gst-python1.0_1.4.0-1.debian.tar.xz
wget http://ftp.de.debian.org/debian/pool/main/g/gst-python1.0/gst-python1.0_1.4.0-1.dsc
wget http://ftp.de.debian.org/debian/pool/main/g/gst-python1.0/gst-python1.0_1.4.0.orig.tar.xz

dpkg-source -x gst-python1.0_1.4.0-1.dsc

cd gst-python1.0-1.4.0

cp -Rf ../../patch/gst-python/* .
#exit 0

patch -p1 < debian/patches/debian_install.patch

#exit 0

dpkg-buildpackage -b -us -uc

cd ..
exit 0
