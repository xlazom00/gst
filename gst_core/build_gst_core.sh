
echo '-----------------------------------------------------------------------------------------------------'
echo 'gst core'
git clone git://anongit.freedesktop.org/gstreamer/gstreamer
cd gstreamer

git checkout remotes/origin/1.4

wget http://archive.ubuntu.com/ubuntu/pool/main/g/gstreamer1.0/gstreamer1.0_1.4.5-1.debian.tar.xz
tar xvvf gstreamer1.0_1.4.5-1.debian.tar.xz
rm gstreamer1.0_1.4.5-1.debian.tar.xz

#exit 0

#cp -Rf ../../patch/gst-core/* .

# rls patch set
#patch -p1 < debian/patches/debian_install_rls.patch
#patch -p1 < debian/patches/debian_install2_rls.patch

./autogen.sh --noconfigure


dpkg-buildpackage -b -us -uc -j4
cd ..

exit 0
