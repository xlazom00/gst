
echo '-----------------------------------------------------------------------------------------------------'
echo 'gst base'
#sudo dpkg -i gstreamer1.0-doc_1.4.0-1_all.deb libgstreamer1.0-dev_1.4.0-1_armhf.deb gir1.2-gstreamer-1.0_1.4.0-1_armhf.deb libgstreamer1.0-0_1.4.0-1_armhf.deb
git clone git://anongit.freedesktop.org/git/gstreamer/gst-plugins-base
cd gst-plugins-base
git checkout remotes/origin/1.4

wget http://archive.ubuntu.com/ubuntu/pool/main/g/gst-plugins-base1.0/gst-plugins-base1.0_1.4.5-1ubuntu1.debian.tar.xz
tar xvvf gst-plugins-base1.0_1.4.5-1ubuntu1.debian.tar.xz
rm gst-plugins-base1.0_1.4.5-1ubuntu1.debian.tar.xz

cp -Rf ../../patch/gst-base/* .

# rls patch set
#patch -p1 < debian/patches/debian_install_rls.patch

#exit 0

./autogen.sh --noconfigure

#exit 0

dpkg-buildpackage -b -us -uc -j4
cd ..

exit 0
